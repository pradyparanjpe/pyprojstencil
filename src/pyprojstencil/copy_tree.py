#!/usr/bin/env python3
# -*- coding:utf-8; mode:python; -*-
#
# Copyright 2021 Pradyumna Paranjape
# This file is part of pyprojstencil.
#
# pyprojstencil is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pyprojstencil is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with pyprojstencil.  If not, see <https://www.gnu.org/licenses/>.
#
"""
Deep copy the templates directory with modifications
"""

import os
import shutil
import stat
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import List

from pyprojstencil.common import edit_modify
from pyprojstencil.configure import PyConfig
from pyprojstencil.errors import MaxRecursion, TemplateMissingError


def _mod_exec(exec_file: os.PathLike) -> None:
    """
    Make file executable

    Args:
        exec_file: path of file to be made executable

    Returns:
        ``None``
    """
    st = os.stat(exec_file)
    os.chmod(exec_file, st.st_mode | stat.S_IEXEC)


def mod_exec(config: PyConfig) -> None:
    """
    make files in the directory "code-aid" executable
    """
    for exe_path in ("code-aid/init_venv.sh", "code-aid/install.sh",
                     "code-aid/coverage.sh", "code-aid/pypi.sh",
                     'code-aid/build_sphinx.sh'):
        _mod_exec(config.project / exe_path)
    main_exec = config.project / f'src/{config.project.name}/__main__.py'
    if main_exec.is_file():
        _mod_exec(main_exec)
    main_exec = config.project / f'{config.project.name}/__main__.py'
    if main_exec.is_file():
        _mod_exec(main_exec)


def tree_copy(config: PyConfig,
              source: os.PathLike,
              destin: os.PathLike,
              skip_deep: bool = False,
              stack: int = 0) -> bool:
    """
    Recursively copy file tree
    - Skips project-specific directories, if found:

      - .git
      - .venv

    - Nodes handling:

      - create directories
      - interpret links and link them
      - :meth:`pyprojstencil.common.edit_modify` and write files

    Args:
        config: pyprojstencil configuration
        source: Path to root of source-tree

    Raises:
        TemplateMissingError: source could not be located
        MaxRecursion: Tree structure is too deep
        OSError: Permission, FileExists, etc

    Returns:
        ``False`` if some nodes were skipped, ``True`` ordinarily
    """
    if stack > 0x80:  # too deep
        if skip_deep:
            return False
        raise MaxRecursion(source)

    ret_code = True
    dest_root = Path(destin)
    dest_root.mkdir(parents=True, exist_ok=True)
    src_root = Path(source)
    if not src_root.is_dir():
        # Source directory does not exist
        raise TemplateMissingError(src_root)

    for node in src_root.glob("*"):
        if node.is_file():
            # file
            # compiled
            if node.name[-4:] == ".pyc":
                continue
            if node.name == 'setup.cfg':
                continue
            (dest_root / node.name.replace("dot.", ".")).write_text(
                edit_modify(node.read_text(), config))
        elif node.is_symlink():
            # relative link
            # NEXT: in python3.9 os.readlink is replaced by Path().readlink
            symlink_target = Path(os.readlink(node)).relative_to(src_root)
            (dest_root / node.name.replace("dot.", ".")).symlink_to(
                (dest_root / symlink_target),
                target_is_directory=Path(os.readlink(node)).is_dir())
        elif node.name in (".git", ".venv", "__pycache__", "licenses"):
            # skip
            continue
        elif node.is_dir() and node.name != 'src':
            # directory: recurse
            dirname = dest_root / node.name.replace("dot.", ".")
            ret_code &= tree_copy(config,
                                  node,
                                  dirname,
                                  skip_deep=skip_deep,
                                  stack=stack + 1)
    return ret_code


def write_src(config: PyConfig,
              src: os.PathLike,
              skip_deep: bool = False,
              stack: int = 0):
    """
    Recursively copy source.

    If project already exists, its copy is left as `PROJECT.src`

    Args:
        config: pyprojstencil configuration
        src: Path to project_dir (typically, 'src')
        project_root: Path to project's root directory

    Raises:
        TemplateMissingError: source could not be located
        MaxRecursion: Tree structure is too deep
        OSError: Permission, FileExists, etc

    Returns:
        Path to temporary location
    """
    src_locs: List[Path] = [Path(src) / 'package_dir']
    if isinstance(config.grab, Path):
        src_locs.append(config.grab)
    if config.project.exists():
        if not config.grab:
            raise FileExistsError(f'{config.project.name} exists')
        config.project.rename(config.project.with_suffix('.src'))
        src_locs.append(config.project.with_suffix('.src'), )
    with TemporaryDirectory('w') as temp_src:
        src_path = Path(temp_src)
        for loc in src_locs:
            if not loc.exists():
                continue
            tree_copy(config, loc, src_path, skip_deep=skip_deep, stack=stack)
        if config.flat_layout:
            shutil.move(src_path, config.project / config.project.name)
        else:
            (config.project / 'src').mkdir(parents=True, exist_ok=True)
            shutil.move(src_path,
                        config.project / f'src/{config.project.name}')


def write_setup(config: PyConfig, source: os.PathLike):
    """
    Write setup.cfg with tweaks

    Args:
        config: configuration
        source: location of template setup file
    """
    setup = edit_modify(Path(source).read_text(), config=config)
    if config.flat_layout:
        setup = setup.replace('package_dir = =src\n', '')
    else:
        setup += '\n[options.packages.find]\nwhere=src\n'
    (config.project / 'setup.cfg').write_text(setup)


def sphinx_docs(config: PyConfig):
    """
    Write docs templates and LICENSE

    Args:
        config: configuration
        source: location of template setup file
    """
    # Add missed files
    (config.project / "LICENSE").write_text(
        edit_modify(config.license.read_text(), config))
    for public in ("docs/_build", "docs/_static", "docs/_templates"):
        (config.project / public).mkdir(parents=True, exist_ok=True)
