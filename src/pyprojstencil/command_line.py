#!/usr/bin/env python3
# -*- coding:utf-8; mode:python; -*-
#
# Copyright 2021 Pradyumna Paranjape
# This file is part of pyprojstencil.
#
# pyprojstencil is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pyprojstencil is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with pyprojstencil.  If not, see <https://www.gnu.org/licenses/>.
#
"""Command line inputs"""

import os
import sys
from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser, Namespace
from datetime import datetime
from pathlib import Path
from typing import Union

from argcomplete import autocomplete

from pyprojstencil.configure import PyConfig, read_config
from pyprojstencil.errors import NoProjectNameError
from pyprojstencil.get_license import get_license

PY_VER = '.'.join(
    (str(ver) for ver in (sys.version_info.major, sys.version_info.minor)))
AUTHOR = os.environ.get('USER', 'AUTHOR')
AUTHOR = AUTHOR[0].upper() + AUTHOR[1:].lower()
YEARS = str(datetime.now().year)


def _grab(cl_input: Union[bool, str]) -> Union[Path, bool]:
    if isinstance(cl_input, bool):
        return cl_input
    if cl_input == '1':
        return True
    return Path(cl_input).resolve()


def filepath(location: str) -> Path:
    """
    Check whether the given location is a valid path to a file

    Args:
        location: Path to be validated

    Returns:
        ``Path`` for location

    Raises:
        FileNotFoundError: location is not an existing file
    """
    location_path = Path(location)
    if location_path.is_file():
        return location_path
    raise FileNotFoundError(f"{location} was not found")


def _cli() -> ArgumentParser:
    """
    Accept command line arguments
    """
    description = '''Create a python project from template.'''
    parser = ArgumentParser(description=description,
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('-c',
                        '--config',
                        help='pre-defined configuration',
                        default=None,
                        type=filepath)
    parser.add_argument('-l',
                        '--license',
                        default='LGPLv3',
                        type=str,
                        help='license name or "custom"')
    parser.add_argument('-y',
                        '--years',
                        default=YEARS,
                        type=str,
                        help=f'copyright period, eg. 2020-2021')
    parser.add_argument('-a',
                        '--author',
                        default=AUTHOR,
                        type=str,
                        help=f'name of author')
    parser.add_argument('-p',
                        '--python-exe',
                        default=PY_VER,
                        dest='pyversion',
                        type=str,
                        help=f'Python version in virtual environment, eg. 3.8')
    parser.add_argument('--flat-layout',
                        action='store_true',
                        help='Use flat (ad-hoc) project layout.')
    parser.add_argument(
        '-g',
        '--grab',
        default=False,
        type=_grab,
        help='If PROJECT exists and GRAB is 1, ' +
        'grab existing location and include its contents as source;' +
        ' If GRAB is path, include it if found.' +
        'DEFAULT behaviour: Throw Error if PROJECT exists.')
    parser.add_argument('project', type=Path, help="Name of python project")
    autocomplete(parser)
    return parser


def cli() -> Namespace:
    """
    Command line arguments
    """
    parser = _cli()
    return parser.parse_args()


def configure() -> PyConfig:
    """
    Set configuration variables

    Returns:
        Configuration variables object

    Raises:
        NoProjectNameError: project name missing
    """
    cli_inputs = cli()
    cli_inputs.license, cli_inputs.license_header = get_license(
        cli_inputs.license)
    project = cli_inputs.project
    if project is None:
        raise NoProjectNameError
    delattr(cli_inputs, 'project')
    config = read_config(project=project, **vars(cli_inputs))
    if config.author is None:
        raise NoProjectNameError
    return config
