*************************
<PROJECT>
*************************

Gist
==========

Source Code Repository
---------------------------

|source| `Repository <https://<GITHOST>.com/<UNAME>/<PROJECT>.git>`__

|pages| `Documentation <https://<UNAME>.<GITHOST>.io/<PROJECT>>`__

Badges
---------

|Pipeline|  |Coverage|  |PyPi Version|  |PyPi Format|  |PyPi Pyversion|


Description
==============

.. Add brief description here

Python project created using `ppstencil <https://pradyparanjpe.gitlab.io/pyprojectstencil>`__

What does it do
--------------------

.. add detailed description here

Some great deed.

.. |Pipeline| image:: https://gitlab.com/<UNAME>/<PROJECT>/badges/master/pipeline.svg

.. |source| image:: https://about.gitlab.com/images/press/logo/svg/gitlab-icon-rgb.svg
   :width: 50
   :target: https://<GITHOST>.com/<UNAME>/<PROJECT>.git

.. |pages| image:: https://about.gitlab.com/images/press/logo/svg/gitlab-logo-100.svg
   :width: 50
   :target: https://<UNAME>.gitlab.io/<PROJECT>

.. |PyPi Version| image:: https://img.shields.io/pypi/v/<PROJECT>
   :target: https://pypi.org/project/<PROJECT>/
   :alt: PyPI - version

.. |PyPi Format| image:: https://img.shields.io/pypi/format/<PROJECT>
   :target: https://pypi.org/project/<PROJECT>/
   :alt: PyPI - format

.. |PyPi Pyversion| image:: https://img.shields.io/pypi/pyversions/<PROJECT>
   :target: https://pypi.org/project/<PROJECT>/
   :alt: PyPi - pyversion

.. |Coverage| image:: https://<GITHOST>.com/<UNAME>/<PROJECT>/badges/master/coverage.svg?skip_ignored=true
