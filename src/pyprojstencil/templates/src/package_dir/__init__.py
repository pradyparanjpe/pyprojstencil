#!/usr/bin/env python<PYVERSION>
# -*- coding: utf-8; mode: python; -*-
<LICENSE_HEADER>
"""<DESCRIPTION>"""

__version__ = '<VERSION>'
__author__ = '<AUTHOR>'
__copyright_years__ = '<YEARS>'
