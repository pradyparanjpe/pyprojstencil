*************************
pyprojstencil
*************************

Gist
==========

Source Code Repository
---------------------------

|source| `Repository <https://gitlab.com/pradyparanjpe/pyprojstencil.git>`__

|pages| `Documentation <https://pradyparanjpe.gitlab.io/pyprojstencil>`__

Badges
---------

|Pipeline|  |Coverage|  |PyPi Version|  |PyPi Format|  |PyPi Pyversion|


Description
==============

Create a python project from template.

What does it do
--------------------

Create a python `project` tree with the following features

  - git init
  - virtualenv ``.venv`` located in `project's` root
  - docs (sphinx-readthedocs)
  - unit tests
  - coverage report
  - code-aid for planning ``plan.org`` and common actions

    - init_venv (initialize venv with linter, importmagic, etc)
    - install (install in local environment)
    - coverage (test code and create a coverage badge)
    - pypi (upload to testpypi repository)


.. |Pipeline| image:: https://gitlab.com/pradyparanjpe/pyprojstencil/badges/master/pipeline.svg

.. |source| image:: https://about.gitlab.com/images/press/logo/svg/gitlab-icon-rgb.svg
   :width: 50
   :target: https://gitlab.com/pradyparanjpe/pyprojstencil.git

.. |pages| image:: https://about.gitlab.com/images/press/logo/svg/gitlab-logo-100.svg
   :width: 50
   :target: https://pradyparanjpe.gitlab.io/pyprojstencil

.. |PyPi Version| image:: https://img.shields.io/pypi/v/pyprojstencil
   :target: https://pypi.org/project/pyprojstencil/
   :alt: PyPI - version

.. |PyPi Format| image:: https://img.shields.io/pypi/format/pyprojstencil
   :target: https://pypi.org/project/pyprojstencil/
   :alt: PyPI - format

.. |PyPi Pyversion| image:: https://img.shields.io/pypi/pyversions/pyprojstencil
   :target: https://pypi.org/project/pyprojstencil/
   :alt: PyPi - pyversion

.. |Coverage| image:: https://gitlab.com/pradyparanjpe/pyprojstencil/badges/master/coverage.svg?skip_ignored=true
